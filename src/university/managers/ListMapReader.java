/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university.managers;

import java.io.File;
import java.util.ArrayList;
import university.Map;
import university.sprites.Container;
import university.sprites.Sprite;

/**
 *
 * @author Tec Construyo Red
 */
public class ListMapReader implements ReaderFile {

    @Override
    public ArrayList<Map> read(String route, Container cont) {
        
        ArrayList<Map> levels = new ArrayList<>();
        File f = null;
        String[] maps;
        try {    
            // create new file
            f = new File(route);
            // array of files and directory
            maps = f.list();
            // for each name in the path array
            for(String path:maps) {
                Map map = new Map(0, 0,cont);
                map.loadMap(route.concat(path));
                levels.add(map);
            }
        } catch(Exception e) {
           // if any error occurs
           e.printStackTrace();
        }
        return levels;
    }
    
}
