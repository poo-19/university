/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university.sprites;

import java.awt.Dimension;

/**
 *
 * @author Tec Construyo Red
 */
public interface Container 
{
    public void refresh();
    public Dimension getBorders();
}
