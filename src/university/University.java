/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import university.managers.ListMapReader;
import university.managers.ReaderWriterScores;
import university.managers.ScoreFileManager;
import university.sprites.Container;
import university.view.MenuFrame;

/**
 *
 * @author Tec Construyo Red
 */
public class University{
    
    private ArrayList<Map> levels;
    private int currentLevel = 0;

    public University(int x, int y, Container c, String name) {
        levels = new ArrayList<>();
        ListMapReader readerMaps = new ListMapReader();
        levels = readerMaps.read("levels/", c);
        levels.forEach((level)->{
            level.setUniversity(this);
            level.setName(name);
        });
        this.currentLevel++;
    }
    
    /**
     * Dibuja el primer nivel.
     * @param g la intancia Graphics de la ventana padre
     */
    public void paintGame(Graphics g){
        levels.get(currentLevel-1).draw(g);
    }

    public void initGame() {
        levels.get(currentLevel-1).startMap();
    }
    
    /**
     * Dibuja el siguiente nivel.
     * @param g la intancia Graphics de la ventana padre
     */
    public void nextLevel(){
        endGame();
        this.currentLevel++;
        try{
            initGame();
        }catch(IndexOutOfBoundsException ex){
            this.currentLevel--;
            levels.get(currentLevel-1).setGameOver(true);
        }
    }
    
    /**
     * Termina el nivel.
     */
    public void endGame(){
        levels.get(currentLevel-1).stopMap();
    }

    public boolean chekGameOver() {
        return levels.get(currentLevel-1).isGameOver();
    }
    
    /**
     * Recibe un evento de tecla para ser enviado al actual mapa y ser manejado por el
     * @param key Código de tecla
     */
    public void keyPressed(int key)
    {
        if(key == KeyEvent.VK_UP |
           key == KeyEvent.VK_DOWN |
           key == KeyEvent.VK_LEFT |
           key == KeyEvent.VK_RIGHT){
            levels.get(currentLevel-1).keyPressed(key);
        }
    }
    
    /**
     * Devuelve los puntajes actuales
     * @return
     */
    public static ArrayList<Score> listScores(){
        ReaderWriterScores scoresManager = new ScoreFileManager();
        ArrayList<Score> list = scoresManager.readScores();
        return list;
    }
    
    /**
     * Envia un puntaje para ser guardado en el historial
     * @param score
     */
    public void addScore(){
        Score score = new Score();
        score.setName(levels.get(currentLevel-1).getName());
        score.setLevel(currentLevel);
        score.setTime(levels.get(currentLevel-1).getTimeLevel());
        
        ReaderWriterScores scoresManager = new ScoreFileManager();
        scoresManager.writeScore(score);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {  
        MenuFrame menuFrame = new MenuFrame();
        menuFrame.setVisible(true);
    }
}
