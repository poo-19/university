/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university.sprites;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author Tec Construyo Red
 */
public class Wall extends Sprite {

    public Wall(int x, int y, Container c) {
        super(x, y, c);
    }

    @Override
    public void draw(Graphics g) {
        Image image = new ImageIcon("images/wall.png").getImage();
        g.drawImage(image, x, y, this.getWidth(), this.getHeight(),Color.GRAY,null);
    }
    
}
