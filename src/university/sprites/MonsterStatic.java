/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university.sprites;


/**
 *
 * @author Tec Construyo Red
 */
public abstract class MonsterStatic extends Monster {

    public MonsterStatic(int x, int y, Container c) {
        super(x, y, c);
    }
    
    @Override
    public abstract void attack(Student student);
}
