/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university.managers;

import java.util.ArrayList;
import university.sprites.Container;
import university.sprites.Sprite;

/**
 *
 * @author Tec Construyo Red
 */
public interface ReaderFile {

    /**
     * Lee un archivo con los datos del mapa y devuelve un listado de figuras (Sprite) para dibujar
     * @param route ruta del archivo a leer
     * @param cont  Container del frame a ser asignado a las figuras
     * @return elements Listado de figuras encontradas en el archivo
     */
    public ArrayList read(String route, Container cont);
}
