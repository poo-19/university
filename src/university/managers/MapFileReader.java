/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university.managers;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import university.sprites.Container;
import university.sprites.Monster;
import university.sprites.MonsterFactory;
import university.sprites.Sprite;
import university.sprites.Wall;

/**
 *
 * @author Tec Construyo Red
 */
public class MapFileReader implements ReaderFile {
    
    private ArrayList<Sprite> elements;

    public MapFileReader() {
        this.elements = new ArrayList<>();
    }

    /**
     * Lee un archivo con los datos del mapa y devuelve un listado de figuras (Sprite) para dibujar
     * @param route ruta del archivo a leer
     * @param cont  Container del frame a ser asignado a las figuras
     * @return elements Listado de figuras encontradas en el archivo
     */
    @Override
    public ArrayList<Sprite> read(String route, Container cont) {
        setDefaultWalls(cont);
        JSONParser parser = new JSONParser();
 
        try {
 
            Object obj = parser.parse(new FileReader(route));

            JSONObject jsonObject = (JSONObject) obj;
 
            String name = (String) jsonObject.get("name");
            String author = (String) jsonObject.get("author");
            JSONArray monsters = (JSONArray) jsonObject.get("monsters");
            JSONArray walls = (JSONArray) jsonObject.get("walls");
 
            System.out.println("Name: " + name);
            System.out.println("Author: " + author);
            System.out.println("\nComponents List:");
            
            monsters.forEach((mon)->{
                try {
                    Object objMon = parser.parse(mon.toString());
                    JSONObject jsonObjectMon = (JSONObject) objMon;
                    this.loadMonster(jsonObjectMon, cont);
                } catch (ParseException ex) {
                    Logger.getLogger(MapFileReader.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            walls.forEach((mon)->{
                try {
                    Object objMon = parser.parse(mon.toString());
                    JSONObject jsonObjectMon = (JSONObject) objMon;
                    this.loadWall(jsonObjectMon, cont);
                } catch (ParseException ex) {
                    Logger.getLogger(MapFileReader.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
 
        } catch (Exception e) {
            e.printStackTrace();
        }        

        return this.elements;
    }
    
    private void loadMonster(JSONObject line, Container cont){
        Monster m = MonsterFactory.create(Integer.parseInt(line.get("type").toString()) ,(String) line.get("color"), Float.parseFloat(line.get("attack").toString()), cont);
        this.elements.add(m);
    }
    
    private void loadWall(JSONObject line, Container cont){
        Wall wall = new Wall(Integer.parseInt(line.get("x").toString()), Integer.parseInt(line.get("y").toString()), cont);
        wall.setHeight(Integer.parseInt(line.get("h").toString()));
        wall.setWidth(Integer.parseInt(line.get("w").toString()));
        this.elements.add(wall);
    }

    private void setDefaultWalls(Container cont) {
        Wall wallLeft = new Wall(3, 25, cont);
        wallLeft.setHeight(cont.getBorders().height-25);
        wallLeft.setWidth(5);
        this.elements.add(wallLeft);
        
        Wall wallTop = new Wall(3,25, cont);
        wallTop.setHeight(5);
        wallTop.setWidth(cont.getBorders().width);
        this.elements.add(wallTop);
        
        Wall wallRight = new Wall(cont.getBorders().width-7, 25, cont);
        wallRight.setHeight(cont.getBorders().height-25);
        wallRight.setWidth(5);
        this.elements.add(wallRight);
        
        Wall wallBottom = new Wall(0, cont.getBorders().height, cont);
        wallBottom.setHeight(5);
        wallBottom.setWidth(cont.getBorders().width);
        this.elements.add(wallBottom);
    }
}
