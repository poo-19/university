/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university.managers;

import java.util.ArrayList;
import university.Score;

/**
 *
 * @author Tec Construyo Red
 */
public interface ReaderWriterScores {
    
    public abstract void writeScore(Score score);
    public abstract ArrayList<Score> readScores();
}
