/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university.sprites;

/**
 *
 * @author Tec Construyo Red
 */
public class MonsterFactory {
    
    public static final int RAT = 1;
    public static final int COCKROACH = 2;
    
    /**
     * Crea un Monster de un tipo especifico, con una posición aleatoria
     * @param type el tipo de Monster a crear
     * @param color el color por defecto
     * @param attack el nivel de daño del Monster
     * @param container el Container padre a asignar al Monster
     * @return m el Monster creado
     */
    public static Monster create(int type, String color, float attack, Container container)
    {
        int width = container.getBorders().width;
        int height = container.getBorders().height;
        int x = (int)(Math.random() * width);
        int y = (int)(Math.random() * height);
        if(x>(container.getBorders().height)-(x/2)){
            x = x-(x-container.getBorders().height);
        }
        if(y>(container.getBorders().width-(y/2))){
            y = y-(y-container.getBorders().width);
        }
        
        Monster m = null;
        
        if(type == RAT)
            m = new Rat(x, y, container);
        else
            m = new Cockroach(x, y, container);
        
        m.setAttackLevel(attack);
        m.setColor(color);
        
        return m;
    }
}
