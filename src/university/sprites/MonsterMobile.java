/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university.sprites;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tec Construyo Red
 */
public abstract class MonsterMobile extends Monster implements Runnable {

    protected Thread thread;
    private boolean reverseX = false;
    private boolean reverseY = false;
    private int step = 10;
    private boolean stoped = true;
    
    public MonsterMobile(int x, int y, Container c) {
        super(x, y, c);
        thread = new Thread(this);
    }

    public boolean isReverseX() {
        return reverseX;
    }

    public void setReverseX(boolean reverse) {
        this.reverseX = reverse;
    }

    public boolean isReverseY() {
        return reverseY;
    }

    public void setReverseY(boolean reverse) {
        this.reverseY = reverse;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public boolean isStoped() {
        return stoped;
    }

    public void setStoped(boolean stoped) {
        this.stoped = stoped;
    }
    
    @Override
    public void run() {        
        while(true)
        {
            if(!stoped){
                if(Math.random() < 0.5){
                    if(y >= (container.getBorders().height-(this.getHeight()+10))){
                        this.setReverseY(true);
                    }else if(y <= (this.getHeight()/2)){
                        this.setReverseY(false);
                    }else{
                       this.setReverseY(Math.random() < 0.3); 
                    }

                    if(!this.isReverseY()){
                        y += step; 
                    }
                    if(this.isReverseY()){
                        y -= step; 
                    }
                }else{
                    if(x >= (container.getBorders().width-(this.getWidth()+10))){
                        this.setReverseX(true);
                    }else if(x <= (this.getWidth()/2)){
                        this.setReverseX(false);
                    }else{
                       this.setReverseX(Math.random() < 0.3); 
                    }
                    if(!this.isReverseX()){
                        x += step; 
                    }
                    if(this.isReverseX()){
                        x -= step; 
                    }
                }

                container.refresh();

                playSound();
                try {
                    Random rn = new Random();
                    Thread.sleep(rn.nextInt(1000) + 1);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MonsterMobile.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MonsterMobile.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public abstract void playSound();
    
}
