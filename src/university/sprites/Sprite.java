/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university.sprites;

import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author Tec Construyo Red
 */
public abstract class Sprite 
{
    protected int x;
    protected int y;
    protected int width;
    protected int height;
    protected Container container;
    
    public Sprite(int x, int y, Container c)
    {
        this.x = x;
        this.y = y;
        this.width = 20;
        this.height = 20;
        container = c;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
    
    public Rectangle getBounds() {
        return new Rectangle(x, y, width, height);
    }
    
    public abstract void draw(Graphics g);
}
