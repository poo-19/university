/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import javax.imageio.ImageIO;
import university.managers.MapFileReader;
import university.managers.ReaderFile;
import university.sprites.Container;
import university.sprites.Ghost;
import university.sprites.Monster;
import university.sprites.MonsterMobile;
import university.sprites.MonsterStatic;
import university.sprites.Sprite;
import university.sprites.Student;
import university.sprites.Wall;

/**
 *
 * @author Tec Construyo Red
 */
public class Map extends Sprite implements Container {
    
    private University university;
    
    private BufferedImage floor;
    private Student student;
    private Ghost ghost;
    private ArrayList<Monster> monsters;
    private ArrayList<Wall> walls;
    private boolean studentGenerated = false;
    private boolean ghostGenerated = false;
    private boolean gameOver = false;
    private Instant stratTime = Instant.now();
    private Instant endTime = null;
    private Timer checkGameTimer = new Timer();
    private TimerTask task = new TimerTask() {
        public void run()
        {
            Float health = student.getHealt();
            if(health <= 0){
                gameOver = true;
                endTime = Instant.now();
            }
        }
    };

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
        endTime = Instant.now();
    }

    public boolean isStudentGenerated() {
        return studentGenerated;
    }

    public boolean isGhostGenerated() {
        return ghostGenerated;
    }
    
    
    public Float getTimeLevel(){
        Duration interval = Duration.between(stratTime, endTime);
        float seconds = (float) interval.getSeconds();
        return seconds;
    }
    

    public void setUniversity(University university) {
        this.university = university;
    }
    
    public void setName(String name){
        student.setName(name);
    }
    
    public String getName(){
        return student.getName();
    }

    public Map(int x, int y, Container c) {
        super(x, y, c);
        this.height = c.getBorders().height;
        this.width = c.getBorders().width;
        monsters = new ArrayList<Monster>();
        walls = new ArrayList<Wall>();
        student = new Student(0, 0, container);
        ghost = new Ghost(container.getBorders().width/2, container.getBorders().height/2, container);
        try {
            floor = ImageIO.read(new File("images/floor.jpg"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Inicia la lectura del mapa y separa las figuras recibidas por le lector segun sus tipos.
     * @param route la ruta del mapa a leer
     */
    public void loadMap(String route) {
        ReaderFile reader = new MapFileReader();
        ArrayList<Sprite> elements = reader.read(route, container);
        elements.forEach((sprite)->{
            if(sprite instanceof Monster){
                this.monsters.add((Monster) sprite);
            }else if(sprite instanceof Wall){
                this.walls.add((Wall) sprite);                
            }
        });
        for (Monster mon : monsters) {
            MonsterMobile mb = (MonsterMobile) mon;
            mb.setStoped(true);
        }
    }
    
    /**
     * inicia las animaciones y sonidos de los Monster
     */
    public void startMap(){
        for (Monster mon : monsters) {
            MonsterMobile mb = (MonsterMobile) mon;
            mb.setStoped(true);
        }
        this.refresh();
        checkGameTimer = new Timer();
        checkGameTimer.schedule(task, 0L ,10L);
    }
    
    /**
     * Detiene las animaciones y sonidos de los Monster
     */
    public void stopMap(){
        for (Monster mon : monsters) {
            MonsterMobile mb = (MonsterMobile) mon;
            mb.setStoped(true);
        }
        checkGameTimer.cancel();
    }
    
    /**
     * Valida si el Student colsiono con alguna figura en el mapa
     * @return Sprite retorna la figura colisionada
     */
    public Sprite checkCollisions() {

        Rectangle r3 = student.getBounds();

        for (Wall wall : walls) {
            Rectangle r2 = wall.getBounds();
            if (r3.intersects(r2)) {
                return wall;
            }
        }

        for (Monster mon : monsters) {
            Rectangle r2 = mon.getBounds();
            if (r3.intersects(r2) && mon instanceof MonsterMobile) {
                return mon;
            }
        }
        
        Rectangle r4 = ghost.getBounds();
        if (r3.intersects(r4)) {
            return ghost;
        }
        return null;
    }
    
    /**
     * recibe un evento de teclado de direccion
     * @param key codigo de tecla de direccion
     */
    public void keyPressed(int key)
    {
        if(key == KeyEvent.VK_UP |
           key == KeyEvent.VK_DOWN |
           key == KeyEvent.VK_LEFT |
           key == KeyEvent.VK_RIGHT){
            student.keyPressed(key,false);
            Sprite sprite = checkCollisions();
            if(sprite!=null && sprite instanceof Wall){
                student.keyPressed(key,true);
            }
            if(sprite!=null && sprite instanceof MonsterMobile){
                MonsterMobile mon = (MonsterMobile) sprite;
                mon.attack(student);
            }
            
            if(sprite!=null && sprite instanceof MonsterStatic){
                this.university.nextLevel();
                this.refresh();
            }
        }
    }
    
    @Override
    public void draw(Graphics g) {
        
        drawStaticSprites(g);
        
        student.draw(g);
        generateStudent();
        
        ghost.draw(g);
        generateGhost(g);
        
        for(Monster m : monsters){
            m.draw(g);
            MonsterMobile mb = (MonsterMobile) m;
            mb.setStoped(false);
        }
    }
    
    public void drawStaticSprites(Graphics g){
        Graphics2D g2d = (Graphics2D) g.create();
        int floorWidth = floor.getWidth();
        int floorHeight = floor.getHeight();
        for (int y = 0; y < getHeight(); y += floorHeight) {
            for (int x = 0; x < getWidth(); x += floorWidth) {
                g2d.drawImage(floor, x, y, null);
            }
        }
        g2d.dispose();

        for(Wall w : walls){
            w.draw(g);
        }
    }

    @Override
    public void refresh() {
        container.refresh();
    }

    @Override
    public Dimension getBorders() {
        return new Dimension(width, height);
    }

    private void generateStudent() {
        int x = 0;
        int y = 0;
        while(checkCollisions()!=null && isStudentGenerated()==false){
            x++;
            y++;
            student.setX(x);
            student.setY(y);
            this.refresh();
            if(checkCollisions()==null){
                this.studentGenerated=true;
            }
        };
    }

    private void generateGhost(Graphics g) {
        if(!isGhostGenerated()){
            int x = container.getBorders().width/2;
            int y = container.getBorders().height/2;
            while((checkCollisions()!=null && checkCollisions() instanceof Wall) && isGhostGenerated()==false){
                x++;
                y++;
                ghost.setX(x);
                ghost.setY(y);
                this.refresh();
                if(checkCollisions()==null){
                    this.ghostGenerated=true;
                }
            };
        }
    }
    
}
