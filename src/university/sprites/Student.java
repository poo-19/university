/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university.sprites;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

/**
 *
 * @author Tec Construyo Red
 */
public class Student extends Sprite{
    private float healt = 100;
    private String name;
    public static final int STEP = 8;
    private Player pl = null;
    
    private final String imageDefault =  "images/walk-paused.gif";
    
    public Student(int x, int y, Container c) {
        super(x, y, c);
        setWidth(31);
        setHeight(50);
    }
    
    public void decreaseHealt(float percentaje){
        float minhealt = (float)width/100;
        float total = healt-(minhealt*(float)percentaje);
        healt = total;
    }

    public float getHealt() {
        return healt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void keyPressed(int key, boolean colisioned)
    {
        if(key == KeyEvent.VK_UP |
           key == KeyEvent.VK_DOWN |
           key == KeyEvent.VK_LEFT |
           key == KeyEvent.VK_RIGHT)
            walk(key, colisioned);
    }
    
    /**
     * Mueve el estudiante hacia una posicion segun la tecla de dirección presionada, 
     * a menos que se encuentre con otro objeto retrocede.
     * @param direction codigo de la tecla de direccion
     * @param colisioned si el movimeinto tuvo una colision
     */
    public void walk(int direction, boolean colisioned)
    {
        if(direction == KeyEvent.VK_UP)
        {
            if(colisioned){
                y += STEP;
                if(y > (container.getBorders().height-(this.getHeight()/2))){
                    y -= STEP;
                }else{
                    soundWalk();
                }
            }else{
                y -= STEP;
                if(y < 0){
                    y += STEP;
                }else{
                    soundWalk();
                }
            }
        }
        
        if(direction == KeyEvent.VK_DOWN)
        {
            if(colisioned){
                y -= STEP;
                if(y < 0){
                    y += STEP;
                }else{
                    soundWalk();
                }
            }else{
                y += STEP;
                if(y > (container.getBorders().height-(this.getHeight()/2))){
                    y -= STEP;
                }else{
                    soundWalk();
                }
            }
        }
        
        if(direction == KeyEvent.VK_LEFT)
        {
            if(colisioned){
                x += STEP;
                if(x > (container.getBorders().width-(this.getWidth()/2))){
                    x -= STEP;
                }else{
                    soundWalk();
                }
            }else{
                x -= STEP;
                if(x < 0){
                    x += STEP;
                }else{
                    soundWalk();
                }
            }
        }
        
        if(direction == KeyEvent.VK_RIGHT)
        {
            if(colisioned){
                x -= STEP;
                if(x < 0){
                    x += STEP;
                }else{
                    soundWalk();
                }
            }else{
                x += STEP;
                if(x > (container.getBorders().width-(this.getWidth()/2))){
                    x -= STEP;
                }else{
                    soundWalk();
                }
            }
        }     
    }
    
    /**
     * Reproduce el sonido de un paso al caminar
     */
    public void soundWalk()
    {        
        container.refresh();
        String bip = "sounds/walk.mp3";
        try {
            if(pl!=null){
               pl.close();
            }
            pl = new Player(new FileInputStream(bip));
            pl.play(20);
        } catch (JavaLayerException | FileNotFoundException ex) {
            Logger.getLogger(Rat.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void draw(Graphics g) 
    {
        Image image = new ImageIcon(this.imageDefault).getImage();
        g.drawImage(image, x, y, width, height,null,null);
        
        //draw rect healt
        g.setColor(Color.red);
        g.drawRect(x, y-10, width, 5);
        g.setColor(Color.green);
        float minhealt = ((float)width/100)*healt;
        g.fillRect(x, y-10, (int)minhealt, 5);
    }
    
}
