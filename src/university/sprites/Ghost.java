/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university.sprites;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author Tec Construyo Red
 */
public class Ghost extends MonsterStatic {

    public Ghost(int x, int y, Container c) {
        super(x, y, c);
        setWidth(40);
        setHeight(45);
    }

    @Override
    public void draw(Graphics g) {
        Image imageDoor = new ImageIcon("images/door.png").getImage();
        g.drawImage(imageDoor, x, y, this.getWidth(), this.getHeight(),null,null);
        
        Image image = new ImageIcon("images/ghost.gif").getImage();
        g.drawImage(image, x, y, this.getWidth(), this.getHeight(),null,null);
    }  

    @Override
    public void attack(Student student) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
