/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university.sprites;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

/**
 *
 * @author Tec Construyo Red
 */
public class Rat extends MonsterMobile{

    public Rat(int x, int y, Container c) {
        super(x, y, c);
        setStep(10);
        setWidth(60);
        setHeight(45);
        setAttackLevel(10);
        thread.start();
    }

    @Override
    public void attack(Student student) {
        student.decreaseHealt(this.getAttackLevel());
    }

    @Override
    public void draw(Graphics g) {
        Image image = new ImageIcon("images/rat.gif").getImage();
        g.drawImage(image, x, y, this.getWidth(), this.getHeight(),null,null);
    }

    @Override
    public void playSound() {
        String bip = "sounds/rat.mp3";
        try {
            Player pl = new Player(new FileInputStream(bip));
            pl.play();
        } catch (JavaLayerException | FileNotFoundException ex) {
            Logger.getLogger(Rat.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
