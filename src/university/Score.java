/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university;

import java.io.Serializable;

/**
 *
 * @author Tec Construyo Red
 */
public class Score implements Serializable {
    
    private String name;
    private float time;
    private int level;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return getName()+" - Level: "+getLevel()+" - Time: "+getTime()+ " seconds";
    }
    
    
}
