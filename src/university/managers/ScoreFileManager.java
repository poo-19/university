/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university.managers;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import university.Score;

/**
 *
 * @author Tec Construyo Red
 */
public class ScoreFileManager implements ReaderWriterScores{
    
    private static final String FILESCORES = "scores.txt";
    
    /**
     * Permite escribir un objeto Score serializable en el archivo de scores.txt de historial de puntajes
     * @param score objeto Score a guardar
     */
    @Override
    public void writeScore(Score score) {
        ArrayList<Score> results = readScores();
        results.add(score);
        
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(FILESCORES);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(results);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ScoreFileManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ScoreFileManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (fos != null){
                try {
                    fos.close();
                } catch (IOException ex) {
                    Logger.getLogger(ScoreFileManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Lee el archivo de Scores.txt y devuelve los puntajes serializados encontrados en el.
     * @return results Listado de los puntajes encontrados
     */
    @Override
    public ArrayList<Score> readScores() {
        ArrayList<Score> results = new ArrayList<Score>();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(FILESCORES);
            ObjectInputStream ois = new ObjectInputStream(fis);
            results = (ArrayList<Score>) ois.readObject();
        } catch (EOFException ignored) {
            // as expected
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ScoreFileManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ScoreFileManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (fis != null)
                try {
                    fis.close();
            } catch (IOException ex) {
                Logger.getLogger(ScoreFileManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("results = " + results);
        return results;
    }
    
}
