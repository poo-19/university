/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package university.sprites;

/**
 *
 * @author Tec Construyo Red
 */
public abstract class Monster extends Sprite{
    
    private float attackLevel;
    private String color;

    public Monster(int x, int y, Container c) {
        super(x, y, c);
    }

    public float getAttackLevel() {
        return attackLevel;
    }

    public void setAttackLevel(float attackLevel) {
        this.attackLevel = attackLevel;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    public abstract void attack(Student student);
    
}
